package org.eclipse.cbi.grac;

public class Example {
    Example() {
        // hide constructor
    }
    public static void hello(String name) {
        System.out.println("Hello " + name + "!");
    }
}
