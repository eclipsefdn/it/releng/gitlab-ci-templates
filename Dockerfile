# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

ARG BASE_IMAGE=ubuntu
ARG BASE_IMAGE_VERSION=latest
FROM ${BASE_IMAGE}:${BASE_IMAGE_VERSION}

ENV DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3002
USER root

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends eatmydata && \
    eatmydata apt-get install -y --no-install-recommends \
      git \
    && eatmydata apt-get clean && rm -rf /var/lib/apt/lists/*