# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

.renovate_rules: &renovate_rules
  rules : 
    - if: $CI_RENOVATE
      when: never
    - when: always

include:
  - local: pipeline.gitlab-ci.yml
    <<: *renovate_rules
  - local: /jobs/workflow.gitlab-ci.yml
  - local: /jobs/renovate.gitlab-ci.yml
  - local: /jobs/scorecard.gitlab-ci.yml
  - local: /jobs/buildkit.gitlab-ci.yml
  - local: /jobs/hadolint.gitlab-ci.yml
  - local: /jobs/download.eclipse.org.gitlab-ci.yml
  - local: /jobs/git.gitlab-ci.yml
  - local: /jobs/repo.eclipse.org.gitlab-ci.yml
  - local: /jobs/matrix.eclipse.org.gitlab-ci.yml
  - local: /jobs/develocity.eclipse.org.gitlab-ci.yml

default:
  image: eclipsecbi/buildpack-deps:noble
  tags:
    - origin:eclipse
    - cluster:okd-c1

stages:
  - compliance
  - build
  - integration
  - buildkit-integration
  - quality
  - test
  - renovate
  - scorecard

variables:  
  CI_REGISTRY_IMAGE: docker.io/eclipsecbi/gitlab-ci-templates
  VAULT_AUTH_PATH: id_token_jwt
  POSTGRESQL_DATABASE: smo
  POSTGRESQL_USER: custom_user
  POSTGRESQL_PASSWORD: custom_pass
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"

# for test purposes
# buildkit: 
#   rules:
#     - when: never
    
buildkit:
  extends: .ef-buildkit
  stage: buildkit-integration
  rules:
    - if: $CI_RENOVATE
      when: never
    - when: always

build-trivy:
  extends: buildkit
  stage: build
  tags:
    -  ctx:small 
  variables:
    KUBERNETES_CPU_REQUEST: "3"
    KUBERNETES_CPU_LIMIT: "5"
    KUBERNETES_MEMORY_REQUEST: "5Gi"
    KUBERNETES_MEMORY_LIMIT: "5Gi"
    BUILD_CONTEXT: src/docker
    BUILD_CONTEXT_CHANGE: src/docker/
    DOCKERFILE_NAME: Dockerfile.trivy
    CI_REGISTRY_IMAGE: docker.io/eclipsecbi/trivy

build-scorecard:
  extends: buildkit
  stage: build
  variables:
    BUILD_CONTEXT: src/docker
    BUILD_CONTEXT_CHANGE: src/docker/
    DOCKERFILE_NAME: Dockerfile.scorecard
    CI_REGISTRY_IMAGE: docker.io/eclipsecbi/scorecard
    IMAGE_TAG: latest

build-gemnasium-maven:
  extends: buildkit
  stage: build
  variables:
    BUILD_CONTEXT: src/docker
    BUILD_CONTEXT_CHANGE: src/docker/
    DOCKERFILE_NAME: Dockerfile.gemnasium-maven
    CI_REGISTRY_IMAGE: docker.io/eclipsecbi/gemnasium-maven

buildkit-okd-c2:
  extends: .buildkit
  stage: buildkit-integration
  tags:
    - origin:eclipse
    - cluster:okd-c2
  variables:
    PUSH_TO_REGISTRY: "false"
  rules:
    - if: $CI_RENOVATE
      when: never
    - when: always

build-with-buildkit:
  extends: buildkit
  variables:
    PUSH_TO_REGISTRY: "false"
    BUILD_CONTEXT: test/docker
    BUILD_CONTEXT_CHANGE: test/docker/

build-with-buildkit-args:
  extends: buildkit
  variables:
    PUSH_TO_REGISTRY: "false"
    BUILD_CONTEXT: test/docker
    BUILD_CONTEXT_CHANGE: test/docker/
    BUILD_ARG: "--opt build-arg:BASE_IMAGE=debian --opt build-arg:BASE_IMAGE_VERSION=latest"

build-with-buildkit-dockerfilename:
  extends: buildkit
  variables:
    PUSH_TO_REGISTRY: "false"
    BUILD_CONTEXT: test/docker
    BUILD_CONTEXT_CHANGE: test/docker/
    DOCKERFILE_NAME: debian.Dockerfile

build-with-buildkit-tag:
  extends: buildkit
  variables:
    PUSH_TO_REGISTRY: "false"
    BUILD_CONTEXT: test/docker
    BUILD_CONTEXT_CHANGE: test/docker/
    IMAGE_TAG: "1.0"

build-with-buildkit-container-name:
  extends: buildkit
  variables:
    PUSH_TO_REGISTRY: "false"
    BUILD_CONTEXT: test/docker
    BUILD_CONTEXT_CHANGE: test/docker/
    CONTAINER_NAME: test

hadolint:
  extends: .hadolint
  <<: *renovate_rules
  stage: quality

hadolint-test:
  extends: hadolint
  variables:
    DOCKERFILE_CONTEXT: test/docker
    DOCKERFILE_CONTEXT_CHANGE: test/docker/

hadolint-test-name:
  extends: hadolint
  variables:
    DOCKERFILE_CONTEXT: test/docker
    DOCKERFILE_CONTEXT_CHANGE: test/docker/
    DOCKERFILE_NAME: debian.Dockerfile

windows-test:
  <<: *renovate_rules
  image: mcr.microsoft.com/powershell:latest
  stage: integration
  needs: []
  script:
    - dir

download-eclipse.org:
  <<: *renovate_rules
  extends: .ef-download-eclipse.org
  stage: integration
  needs: []

git-test:
  <<: *renovate_rules
  extends: .ef-git-github
  stage: integration
  needs: []
  script:
    - git clone https://github.com/eclipse-cbi/best-practices.git
    - cd best-practices
    - touch new_file.txt
    - git add new_file.txt
    - git commit -m "Added new_file.txt"
    - git show

repo.eclipse.org-test:
  <<: *renovate_rules
  stage: integration
  needs: []
  image: maven:3.9.9-eclipse-temurin-21
  extends: .ef-repo-eclipse-org
  variables:
    MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
    MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  script:
    - mvn $MAVEN_CLI_OPTS deploy -s settings.xml

develocity.eclipse.org-mvn-test:
  <<: *renovate_rules
  stage: integration
  needs: []
  extends: 
    - .ef-repo-eclipse-org
    - .ef-build-develocity-maven

develocity.eclipse.org-gradle-test:
  <<: *renovate_rules
  stage: integration
  needs: []
  extends: 
    - .ef-repo-eclipse-org
    - .ef-build-develocity-gradle

matrix.eclipse.org-test:
  <<: *renovate_rules
  stage: integration
  needs: []
  extends: .ef-matrix
  script:
    - |
      curl -s -H "Authorization: Bearer $MATRIX_ACCESS_TOKEN" "https://matrix.eclipse.org/_matrix/client/r0/publicRooms"
      
gitlab-services-integration:
  <<: *renovate_rules
  stage: integration
  needs: []
  services:
    - name: quay.io/sclorg/postgresql-15-c9s
      alias: postgres
  image: postgres
  script:
    - export PGPASSWORD=$POSTGRES_PASSWORD
    - psql -h "postgres" -U "$POSTGRESQL_USER" -d "$POSTGRESQL_DATABASE" -c "SELECT 'OK' AS status;"

scorecard:
  extends: .ef-scorecard